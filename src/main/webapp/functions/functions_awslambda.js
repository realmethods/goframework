'use strict';
 /*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
/**
 * AWS Lambda Function proxy delegate functions.
 * <p>
 * These functions implement the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the availability of related services in the case of a business related service failing.</li>
 * <li>Exposes a simpler, uniform interface to the business tier, making it easy for clients to consume a business data.</li>
 * <li>Hides the communication protocol that may be required to fulfill business related services.</li>
 * <li>Hides the details of how business data is persisted.</li>
 * </ol>
 * <p>
 * @author 
 */

/**
 * function to create the provided Player data raising an exception if it fails
 */
exports.createPlayer = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createPlayer - null Player provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreatePlayer( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createPlayer - unable to create Player" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreatePlayer( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Player", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreatePlayer - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Player data via a supplied Player primary key.
 */
exports.getPlayer = function(event, context, callback) {
    var key = 	{ 
    				playerId : 0
    			};                
	
	key.playerId = event.playerId;
    
    try {
        innerGetPlayer( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createPlayer - unable to locate Player with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetPlayer( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Player", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetPlayer - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Player data.
 */
exports.savePlayer = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createPlayer - null Player provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.playerId ) ) {
        try {                    
            innerSavePlayer(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createPlayer - unable to save Player " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Player due to it having a null PlayerPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSavePlayer( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Player", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSavePlayer - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Players
 */
exports.getAllPlayer = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllPlayer(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createPlayer - failed to getAllPlayer " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllPlayer(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Player", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllPlayer - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Player using its provided primary key.
 */
exports.deletePlayer = function(event, context, callback) {
    var key = 	{ 
    				playerId : 0
    			};                
	
	key.playerId = event.playerId;
	
    try {                    
       	innerDeletePlayer(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createPlayer - Unable to delete Player using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeletePlayer( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Player", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeletePlayer - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
 

/**
 * function to create the provided League data raising an exception if it fails
 */
exports.createLeague = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createLeague - null League provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateLeague( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createLeague - unable to create League" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateLeague( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("League", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateLeague - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the League data via a supplied League primary key.
 */
exports.getLeague = function(event, context, callback) {
    var key = 	{ 
    				leagueId : 0
    			};                
	
	key.leagueId = event.leagueId;
    
    try {
        innerGetLeague( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createLeague - unable to locate League with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetLeague( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("League", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetLeague - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided League data.
 */
exports.saveLeague = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createLeague - null League provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.leagueId ) ) {
        try {                    
            innerSaveLeague(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createLeague - unable to save League " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create League due to it having a null LeaguePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveLeague( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("League", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveLeague - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Leagues
 */
exports.getAllLeague = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllLeague(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createLeague - failed to getAllLeague " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllLeague(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("League", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllLeague - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated League using its provided primary key.
 */
exports.deleteLeague = function(event, context, callback) {
    var key = 	{ 
    				leagueId : 0
    			};                
	
	key.leagueId = event.leagueId;
	
    try {                    
       	innerDeleteLeague(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createLeague - Unable to delete League using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteLeague( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("League", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteLeague - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Players on a League
 * @param parentKey
 */
exports.getPlayersOnLeague = function(event, context, callback) {
    var keys = 	event.body;
    callGet("League", "loadPlayers", keys, event, callback);
};
    
/**
 * function to save multiple Player entities as the Players 
 * of the relevant League associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignPlayersOnLeague = function(event, context, callback) {
    var keys = 	event.body;
    callGet("League", "savePlayers", keys, event, callback);
};

/**
 * function to delete multiple Player entities as the Players 
 * of the relevant League associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deletePlayersOnLeague = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("League", "deletePlayers", keys, event, callback);
};

 

/**
 * function to create the provided Tournament data raising an exception if it fails
 */
exports.createTournament = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createTournament - null Tournament provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateTournament( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTournament - unable to create Tournament" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateTournament( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Tournament", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateTournament - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Tournament data via a supplied Tournament primary key.
 */
exports.getTournament = function(event, context, callback) {
    var key = 	{ 
    				tournamentId : 0
    			};                
	
	key.tournamentId = event.tournamentId;
    
    try {
        innerGetTournament( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createTournament - unable to locate Tournament with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetTournament( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Tournament", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetTournament - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Tournament data.
 */
exports.saveTournament = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createTournament - null Tournament provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.tournamentId ) ) {
        try {                    
            innerSaveTournament(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createTournament - unable to save Tournament " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Tournament due to it having a null TournamentPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveTournament( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Tournament", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveTournament - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Tournaments
 */
exports.getAllTournament = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllTournament(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createTournament - failed to getAllTournament " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllTournament(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Tournament", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllTournament - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Tournament using its provided primary key.
 */
exports.deleteTournament = function(event, context, callback) {
    var key = 	{ 
    				tournamentId : 0
    			};                
	
	key.tournamentId = event.tournamentId;
	
    try {                    
       	innerDeleteTournament(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTournament - Unable to delete Tournament using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteTournament( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Tournament", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteTournament - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Matchups on a Tournament
 * @param parentKey
 */
exports.getMatchupsOnTournament = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Tournament", "loadMatchups", keys, event, callback);
};
    
/**
 * function to save multiple Matchup entities as the Matchups 
 * of the relevant Tournament associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignMatchupsOnTournament = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Tournament", "saveMatchups", keys, event, callback);
};

/**
 * function to delete multiple Matchup entities as the Matchups 
 * of the relevant Tournament associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteMatchupsOnTournament = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Tournament", "deleteMatchups", keys, event, callback);
};

 

/**
 * function to create the provided Matchup data raising an exception if it fails
 */
exports.createMatchup = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createMatchup - null Matchup provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateMatchup( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createMatchup - unable to create Matchup" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateMatchup( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Matchup", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateMatchup - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Matchup data via a supplied Matchup primary key.
 */
exports.getMatchup = function(event, context, callback) {
    var key = 	{ 
    				matchupId : 0
    			};                
	
	key.matchupId = event.matchupId;
    
    try {
        innerGetMatchup( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createMatchup - unable to locate Matchup with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetMatchup( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Matchup", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetMatchup - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Matchup data.
 */
exports.saveMatchup = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createMatchup - null Matchup provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.matchupId ) ) {
        try {                    
            innerSaveMatchup(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createMatchup - unable to save Matchup " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Matchup due to it having a null MatchupPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveMatchup( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Matchup", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveMatchup - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Matchups
 */
exports.getAllMatchup = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllMatchup(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createMatchup - failed to getAllMatchup " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllMatchup(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Matchup", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllMatchup - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Matchup using its provided primary key.
 */
exports.deleteMatchup = function(event, context, callback) {
    var key = 	{ 
    				matchupId : 0
    			};                
	
	key.matchupId = event.matchupId;
	
    try {                    
       	innerDeleteMatchup(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createMatchup - Unable to delete Matchup using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteMatchup( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Matchup", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteMatchup - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Games on a Matchup
 * @param parentKey
 */
exports.getGamesOnMatchup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Matchup", "loadGames", keys, event, callback);
};
    
/**
 * function to save multiple Game entities as the Games 
 * of the relevant Matchup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignGamesOnMatchup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Matchup", "saveGames", keys, event, callback);
};

/**
 * function to delete multiple Game entities as the Games 
 * of the relevant Matchup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteGamesOnMatchup = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Matchup", "deleteGames", keys, event, callback);
};

 

/**
 * function to create the provided Game data raising an exception if it fails
 */
exports.createGame = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createGame - null Game provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateGame( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createGame - unable to create Game" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateGame( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Game", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateGame - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Game data via a supplied Game primary key.
 */
exports.getGame = function(event, context, callback) {
    var key = 	{ 
    				gameId : 0
    			};                
	
	key.gameId = event.gameId;
    
    try {
        innerGetGame( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createGame - unable to locate Game with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetGame( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Game", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetGame - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Game data.
 */
exports.saveGame = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createGame - null Game provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.gameId ) ) {
        try {                    
            innerSaveGame(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createGame - unable to save Game " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Game due to it having a null GamePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveGame( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Game", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveGame - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Games
 */
exports.getAllGame = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllGame(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createGame - failed to getAllGame " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllGame(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Game", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllGame - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Game using its provided primary key.
 */
exports.deleteGame = function(event, context, callback) {
    var key = 	{ 
    				gameId : 0
    			};                
	
	key.gameId = event.gameId;
	
    try {                    
       	innerDeleteGame(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createGame - Unable to delete Game using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteGame( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Game", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteGame - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the Matchup using the provided primary key of a Game
 */
exports.getMatchupOnGame = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Game", "loadMatchup", keys, event, callback );
};

/**
 * function to assign the Matchup on a Game using the provided primary key of a Matchup
 */
exports.saveMatchupOnGame = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Game", "saveMatchup", keys, event, callback);
};

/**
 * function to remove the assignment of the Matchup on a Game
 */
exports.deleteMatchupOnGame = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Game", "deleteMatchup", keys, event, callback);
};
		
/**
 * function to get the Player using the provided primary key of a Game
 */
exports.getPlayerOnGame = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Game", "loadPlayer", keys, event, callback );
};

/**
 * function to assign the Player on a Game using the provided primary key of a Player
 */
exports.savePlayerOnGame = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Game", "savePlayer", keys, event, callback);
};

/**
 * function to remove the assignment of the Player on a Game
 */
exports.deletePlayerOnGame = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Game", "deletePlayer", keys, event, callback);
};
		
 
 

/**
 * function to create the provided Alley data raising an exception if it fails
 */
exports.createAlley = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createAlley - null Alley provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateAlley( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAlley - unable to create Alley" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateAlley( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Alley", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateAlley - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Alley data via a supplied Alley primary key.
 */
exports.getAlley = function(event, context, callback) {
    var key = 	{ 
    				alleyId : 0
    			};                
	
	key.alleyId = event.alleyId;
    
    try {
        innerGetAlley( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createAlley - unable to locate Alley with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetAlley( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Alley", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetAlley - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Alley data.
 */
exports.saveAlley = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createAlley - null Alley provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.alleyId ) ) {
        try {                    
            innerSaveAlley(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createAlley - unable to save Alley " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Alley due to it having a null AlleyPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveAlley( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Alley", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveAlley - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Alleys
 */
exports.getAllAlley = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllAlley(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createAlley - failed to getAllAlley " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllAlley(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Alley", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllAlley - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Alley using its provided primary key.
 */
exports.deleteAlley = function(event, context, callback) {
    var key = 	{ 
    				alleyId : 0
    			};                
	
	key.alleyId = event.alleyId;
	
    try {                    
       	innerDeleteAlley(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAlley - Unable to delete Alley using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteAlley( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Alley", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteAlley - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Leagues on a Alley
 * @param parentKey
 */
exports.getLeaguesOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "loadLeagues", keys, event, callback);
};
    
/**
 * function to save multiple League entities as the Leagues 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignLeaguesOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "saveLeagues", keys, event, callback);
};

/**
 * function to delete multiple League entities as the Leagues 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteLeaguesOnAlley = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Alley", "deleteLeagues", keys, event, callback);
};

/**
 * function to retrieve the Tournaments on a Alley
 * @param parentKey
 */
exports.getTournamentsOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "loadTournaments", keys, event, callback);
};
    
/**
 * function to save multiple Tournament entities as the Tournaments 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignTournamentsOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "saveTournaments", keys, event, callback);
};

/**
 * function to delete multiple Tournament entities as the Tournaments 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteTournamentsOnAlley = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Alley", "deleteTournaments", keys, event, callback);
};

/**
 * function to retrieve the Lanes on a Alley
 * @param parentKey
 */
exports.getLanesOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "loadLanes", keys, event, callback);
};
    
/**
 * function to save multiple Lane entities as the Lanes 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignLanesOnAlley = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Alley", "saveLanes", keys, event, callback);
};

/**
 * function to delete multiple Lane entities as the Lanes 
 * of the relevant Alley associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteLanesOnAlley = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Alley", "deleteLanes", keys, event, callback);
};

 

/**
 * function to create the provided Lane data raising an exception if it fails
 */
exports.createLane = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createLane - null Lane provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateLane( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createLane - unable to create Lane" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateLane( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Lane", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateLane - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Lane data via a supplied Lane primary key.
 */
exports.getLane = function(event, context, callback) {
    var key = 	{ 
    				laneId : 0
    			};                
	
	key.laneId = event.laneId;
    
    try {
        innerGetLane( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createLane - unable to locate Lane with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetLane( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Lane", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetLane - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Lane data.
 */
exports.saveLane = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createLane - null Lane provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.laneId ) ) {
        try {                    
            innerSaveLane(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createLane - unable to save Lane " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Lane due to it having a null LanePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveLane( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Lane", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveLane - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Lanes
 */
exports.getAllLane = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllLane(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createLane - failed to getAllLane " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllLane(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Lane", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllLane - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Lane using its provided primary key.
 */
exports.deleteLane = function(event, context, callback) {
    var key = 	{ 
    				laneId : 0
    			};                
	
	key.laneId = event.laneId;
	
    try {                    
       	innerDeleteLane(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createLane - Unable to delete Lane using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteLane( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Lane", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteLane - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
 
 		
/**
 * internal function used to handle HTTP request to the RESTful API
 */		
function callGet( packageName, actionName, data, event, callback ) {
	return( call( packageName, actionName, data, event, callback,  'GET' ) );
};

function callPut( packageName, actionName, data, event, callback ) {
	return( call( packageName, actionName, data, event, callback, 'PUT' ) );
};

function call( packageName, actionName, data, event, callback, method) {
	var querystring	= require('querystring');
	var https 		= require('http');
	var host 		= RESTFUL_API_DAO_URL;
	var endPoint 	= "/" + packageName + "/" + actionName;
  	var dataString 	= JSON.stringify(data);
	var headers 	= {};
	
	console.log("datastring is " + dataString );
	
	if (method == 'GET') {
		endPoint += '?' + querystring.stringify(data);
	}
	else {
		endPoint += '?' + dataString;	
		headers = {
			'Content-Type': 'application/json',
			'Content-Length': dataString.length
		};
	}
		
  	var options = {
  		hostname: host,
    	path: endPoint,
    	method: "GET",
    	port: PORT,
    	headers: headers
  	};

  	console.log( options );
	var responseString = '';
	var req = https.request(options, function(res) {
    	res.setEncoding('utf-8');
    	res.on('data', function(data) {
      		responseString += data;
    	});
       	res.on('end', function() {    
	    	callback(null, JSON.parse(responseString));	
     	});
	});
	
	req.write( dataString );
	req.end();
  		
};

function getContextDetails( context, error ) {
	var errMsg = '';
	if ( exists( context ) ) {
		errMsg = errMsg + 'remaining time = ' + context.getRemainingTimeInMillis()
	    				+ ', functionName = ' +  context.functionName
	    				+ ', AWSrequestID = ' + context.awsRequestId
	    				+ ', logGroupName = ' + context.log_group_name
	    				+ ', logStreamName = ' + context.log_stream_name
	    				+ ', clientContext = ' + context.clientContext;
	    if ( exists( context.identity ) ) 
	        errMsg = errMsg + ', Cognito identity ID = ' + context.identity.cognitoIdentityId;
	}
		        	
	if ( exits( error ) )
		errMsg = errMsg + ', ' + error.message;
		
    return( errMsg );
};

function exists( objToTest ) {
	if (null == objToTest) 
		return false;
  	if ("undefined" == typeof(objToTest)) 
  		return false;
  	return true;
};

// global vars				   
var errHandler = function(err) {
    console.log(err);
};

var RESTFUL_API_DAO_URL 	= process.env.delegateDAOHost;
var PORT 					= process.env.delegateDAOPort;
var KINESIS_STREAM_NAME		= process.env.kinesisStreamName;