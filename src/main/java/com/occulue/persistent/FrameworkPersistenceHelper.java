/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.persistent;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.*;

import org.hibernate.service.*;

import java.util.logging.Level;
import java.util.logging.Logger;


public class FrameworkPersistenceHelper {
    private static final ThreadLocal session = new ThreadLocal();
    private static FrameworkPersistenceHelper self = null;
    private static Configuration cfg = null;
    private static SessionFactory sessionFactory;

    static {
        createSessionFactory();
    }

    // attributes
    private static final Logger LOGGER = Logger.getLogger(FrameworkPersistenceHelper.class.getName());

    public static FrameworkPersistenceHelper self() {
        if (self == null) {
            self = new FrameworkPersistenceHelper();
        }

        return (self);
    }

    public void jumpStart() {
        // no_op
    }

    public SessionFactory getSessionFactory() {
        if ((sessionFactory == null) || sessionFactory.isClosed()) {
            self().createSessionFactory();
        }

        return sessionFactory;
    }

    public Session getCurrentSession() {
        Session s = (Session) session.get();

        // Open a new Session, if this Thread has none yet or has been closed or disconnected
        if ((s == null) || (s.isOpen() == false) || (s.isConnected() == false)) {
            // Note: dynamically create the class Interceptor and apply here,			
            // if one is in use ...			
            s = getSessionFactory().openSession( /*FrameworkHibernatorInterceptorFactory.getInstance().getHibernateInterceptor()*/
                );
            session.set(s);
            s.setFlushMode(FlushMode.COMMIT);
        }

        return s;
    }

    public void closeSession() {
        Session s = (Session) session.get();
        session.set(null);

        if (s != null) {
            s.close();
        }
    }

    protected static void createSessionFactory() {
        try {
            // up front, create the SessionFactory from hibernate.cfg.xml since it takes a bit to load
            if (cfg == null) {
                cfg = new Configuration().configure("hibernate.cfg.xml");
            }

            ServiceRegistryBuilder serviceRegistryBuilder = new ServiceRegistryBuilder().applySettings(cfg.getProperties());
            sessionFactory = cfg.buildSessionFactory(serviceRegistryBuilder.buildServiceRegistry());
        } catch (Exception ex) {
            sessionFactory = null;
            // Make sure you log the exception, as it might be swallowed
            // Create the SessionFactory from hibernate.cfg.xml
            System.out.println("Warning!!! - Hibernate Session unitialized..." +
                ex.toString());

            //ex.printStackTrace();
        }
    }
}
