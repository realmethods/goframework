/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.occulue.bo.*;
import com.occulue.bo.Base;

import com.occulue.common.JsonTransformer;

import com.occulue.delegate.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.io.StringWriter;

import java.text.SimpleDateFormat;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * Implements Struts action processing for business entity Player.
 *
 * @author
 */
public class PlayerRestService extends BaseRestService {
    private static final Logger LOGGER = Logger.getLogger(BaseRestService.class.getName());

    //************************************************************************    
    // Attributes
    //************************************************************************
    private Player player = null;

    public PlayerRestService() {
    }

    /**
     * Handles saving a Player BO.  if not key provided, calls create, otherwise calls save
     * @exception        ProcessingException
     */
    protected Player save() throws ProcessingException {
        // doing it here helps
        getPlayer();

        LOGGER.info("Player.save() on - " + player);

        if (hasPrimaryKey()) {
            return (update());
        } else {
            return (create());
        }
    }

    /**
     * Returns true if the player is non-null and has it's primary key field(s) set
     * @return                boolean
     */
    protected boolean hasPrimaryKey() {
        boolean hasPK = false;

        if ((player != null) &&
                (player.getPlayerPrimaryKey().hasBeenAssigned() == true)) {
            hasPK = true;
        }

        return (hasPK);
    }

    /**
     * Handles updating a Player BO
     * @return                Player
     * @exception        ProcessingException
     */
    protected Player update() throws ProcessingException {
        // store provided data
        Player tmp = player;

        // load actual data from storage
        loadHelper(player.getPlayerPrimaryKey());

        // copy provided data into actual data
        player.copyShallow(tmp);

        try {
            // create the PlayerBusiness Delegate            
            PlayerBusinessDelegate delegate = PlayerBusinessDelegate.getPlayerInstance();
            this.player = delegate.savePlayer(player);

            if (this.player != null) {
                LOGGER.info(
                    "PlayerRestService:update() - successfully updated Player - " +
                    player.toString());
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:update() - successfully update Player - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.player;
    }

    /**
     * Handles creating a Player BO
     * @return                Player
     */
    protected Player create() throws ProcessingException {
        try {
            player = getPlayer();
            this.player = PlayerBusinessDelegate.getPlayerInstance()
                                                .createPlayer(player);
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:create() - exception Player - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.player;
    }

    /**
     * Handles deleting a Player BO
     * @exception        ProcessingException
     */
    protected void delete() throws ProcessingException {
        try {
            PlayerBusinessDelegate delegate = PlayerBusinessDelegate.getPlayerInstance();

            Long[] childIds = getChildIds();

            if ((childIds == null) || (childIds.length == 0)) {
                PlayerPrimaryKey pk = getPlayerPrimaryKey();

                if (pk == null) {
                    Long playerId = parseId("playerId");
                    pk = new PlayerPrimaryKey(playerId);
                }

                delegate.delete(pk);

                LOGGER.info(
                    "PlayerRestService:delete() - successfully deleted Player with key " +
                    pk.valuesAsCollection());
            } else {
                for (Long id : childIds) {
                    try {
                        delegate.delete(new PlayerPrimaryKey(id));
                    } catch (Throwable exc) {
                        signalBadRequest();

                        String errMsg = "PlayerRestService:delete() - " +
                            exc.getMessage();
                        LOGGER.severe(errMsg);
                        throw new ProcessingException(errMsg);
                    }
                }
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:delete() - " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }
    }

    /**
     * Handles loading a Player BO
     * @param                Long playerId
     * @exception        ProcessingException
     * @return                Player
     */
    protected Player load() throws ProcessingException {
        PlayerPrimaryKey pk = null;
        Long playerId = null;

        try {
            pk = getPlayerPrimaryKey();

            if (pk == null) {
                playerId = parseId("playerId");
                LOGGER.info("Player.load pk is " + playerId);

                if (playerId != null) {
                    pk = new PlayerPrimaryKey(playerId);
                } else {
                    signalBadRequest();

                    String errMsg = "PlayerRestService:load() - unable to locate the primary key as an attribute or a selection for - " +
                        player.toString();
                    LOGGER.severe(errMsg);
                    throw new ProcessingException(errMsg);
                }
            }

            loadHelper(pk);

            // load the contained instance of Player
            this.player = PlayerBusinessDelegate.getPlayerInstance()
                                                .getPlayer(pk);

            LOGGER.info("PlayerRestService:load() - successfully loaded - " +
                this.player.toString());
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:load() - failed to load Player using Id " +
                playerId + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return player;
    }

    /**
     * Handles loading all Player business objects
     * @return                List<Player>
     * @exception        ProcessingException
     */
    protected List<Player> loadAll() throws ProcessingException {
        List<Player> playerList = null;

        try {
            // load the Player
            playerList = PlayerBusinessDelegate.getPlayerInstance()
                                               .getAllPlayer();

            if (playerList != null) {
                LOGGER.info(
                    "PlayerRestService:loadAllPlayer() - successfully loaded all Players");
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:loadAll() - failed to load all Players - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return playerList;
    }

    // findAllBy methods
    protected Player loadHelper(PlayerPrimaryKey pk) throws ProcessingException {
        try {
            LOGGER.info("Player.loadHelper primary key is " + pk);

            if (pk != null) {
                // load the contained instance of Player
                this.player = PlayerBusinessDelegate.getPlayerInstance()
                                                    .getPlayer(pk);

                LOGGER.info(
                    "PlayerRestService:loadHelper() - successfully loaded - " +
                    this.player.toString());
            } else {
                signalBadRequest();

                String errMsg = "PlayerRestService:loadHelper() - null primary key provided.";
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "PlayerRestService:load() - failed to load Player using pk " +
                pk + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return player;
    }

    // overloads from BaseRestService

    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
    public Object handleExec(String action, spark.Response response,
        spark.Request request) throws ProcessingException {
        // store locally
        this.response = response;
        this.request = request;

        if (action == null) {
            signalBadRequest();
            throw new ProcessingException();
        }

        Object returnVal = null;

        switch (action) {
        case "save":
            returnVal = save();

            break;

        case "load":
            returnVal = load();

            break;

        case "delete":
            delete();

            break;

        case "loadAll":
        case "viewAll":
            returnVal = loadAll();

            break;

        default:
            signalBadRequest();
            throw new ProcessingException(
                "Player.execute(...) - unable to handle action " + action);
        }

        return returnVal;
    }

    /**
     * Uses ObjectMapper to map from Json to a Player. Found in the request body.
     *
     * @return Player
     */
    private Player getPlayer() {
        if (player == null) {
            player = (Player) getObjectFromRequest(Player.class);
        }

        return (player);
    }

    /**
     *
     * @return PlayerPrimaryKey
     */
    private PlayerPrimaryKey getPlayerPrimaryKey() {
        return (PlayerPrimaryKey) getObjectFromRequest(PlayerPrimaryKey.class);
    }

    protected String getSubclassName() {
        return ("PlayerRestService");
    }
}
