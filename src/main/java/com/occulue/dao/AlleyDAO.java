/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Alley.
 *
 * @author
 */

// AIB : #getDAOClassDecl()
public class AlleyDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Alley.class.getName());

    /**
     * default constructor
     */
    public AlleyDAO() {
    }

    /**
     * Retrieves a Alley from the persistent store, using the provided primary key.
     * If no match is found, a null Alley is returned.
     * <p>
     * @param       pk
     * @return      Alley
     * @exception   ProcessingException
     */
    public Alley findAlley(AlleyPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "AlleyDAO.findAlley(...) cannot have a null primary key argument");
        }

        Query query = null;
        Alley businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Alley as alley where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("alley.alleyId = " + pk.getAlleyId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Alley();
                businessObject.copy((Alley) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "AlleyDAO.findAlley failed for primary key " + pk + " - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AlleyDAO.findAlley - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Alleys
     * @return                ArrayList<Alley>
     * @exception   ProcessingException
     */
    public ArrayList<Alley> findAllAlley() throws ProcessingException {
        ArrayList<Alley> list = new ArrayList<Alley>();
        ArrayList<Alley> refList = new ArrayList<Alley>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Alley");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Alley>) query.list();

                Alley tmp = null;

                for (Alley listEntry : list) {
                    tmp = new Alley();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("AlleyDAO.findAllAlley failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AlleyDAO.findAllAlley - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("AlleyDAO:findAllAlleys() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Alley into the persistent store.
     * @param       businessObject
     * @return      newly persisted Alley
     * @exception   ProcessingException
     */
    public Alley createAlley(Alley businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AlleyDAO.createAlley - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AlleyDAO.createAlley failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AlleyDAO.createAlley - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Alley to the persistent store.
     *
     * @param       businessObject
     * @return      Alley        stored entity
     * @exception   ProcessingException
     */
    public Alley saveAlley(Alley businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AlleyDAO.saveAlley - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AlleyDAO.saveAlley failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AlleyDAO.saveAlley - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Alley from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteAlley(AlleyPrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Alley bo = findAlley(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AlleyDAO.deleteAlley - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AlleyDAO.deleteAlley failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AlleyDAO.deleteAlley - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
