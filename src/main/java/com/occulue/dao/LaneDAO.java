/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Lane.
 *
 * @author
 */

// AIB : #getDAOClassDecl()
public class LaneDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Lane.class.getName());

    /**
     * default constructor
     */
    public LaneDAO() {
    }

    /**
     * Retrieves a Lane from the persistent store, using the provided primary key.
     * If no match is found, a null Lane is returned.
     * <p>
     * @param       pk
     * @return      Lane
     * @exception   ProcessingException
     */
    public Lane findLane(LanePrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "LaneDAO.findLane(...) cannot have a null primary key argument");
        }

        Query query = null;
        Lane businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Lane as lane where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("lane.laneId = " + pk.getLaneId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Lane();
                businessObject.copy((Lane) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "LaneDAO.findLane failed for primary key " + pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "LaneDAO.findLane - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Lanes
     * @return                ArrayList<Lane>
     * @exception   ProcessingException
     */
    public ArrayList<Lane> findAllLane() throws ProcessingException {
        ArrayList<Lane> list = new ArrayList<Lane>();
        ArrayList<Lane> refList = new ArrayList<Lane>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Lane");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Lane>) query.list();

                Lane tmp = null;

                for (Lane listEntry : list) {
                    tmp = new Lane();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("LaneDAO.findAllLane failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "LaneDAO.findAllLane - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("LaneDAO:findAllLanes() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Lane into the persistent store.
     * @param       businessObject
     * @return      newly persisted Lane
     * @exception   ProcessingException
     */
    public Lane createLane(Lane businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "LaneDAO.createLane - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("LaneDAO.createLane failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "LaneDAO.createLane - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Lane to the persistent store.
     *
     * @param       businessObject
     * @return      Lane        stored entity
     * @exception   ProcessingException
     */
    public Lane saveLane(Lane businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "LaneDAO.saveLane - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("LaneDAO.saveLane failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "LaneDAO.saveLane - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Lane from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteLane(LanePrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Lane bo = findLane(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "LaneDAO.deleteLane - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("LaneDAO.deleteLane failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "LaneDAO.deleteLane - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
