/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.*;

import com.occulue.primarykey.*;

import java.io.IOException;

import java.util.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Alley business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Alley related services in the case of a Alley business related service failing.</li>
 * <li>Exposes a simpler, uniform Alley interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Alley business related services.</li>
 * </ol>
 * <p>
 * @author
 */
public class AlleyBusinessDelegate extends BaseBusinessDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    /**
     * Singleton instance
     */
    protected static AlleyBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Alley.class.getName());

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public AlleyBusinessDelegate() {
    }

    /**
         * Alley Business Delegate Factory Method
         *
         * Returns a singleton instance of AlleyBusinessDelegate().
         * All methods are expected to be self-sufficient.
         *
         * @return         AlleyBusinessDelegate
         */
    public static AlleyBusinessDelegate getAlleyInstance() {
        if (singleton == null) {
            singleton = new AlleyBusinessDelegate();
        }

        return (singleton);
    }

    /**
     * Method to retrieve the Alley via an AlleyPrimaryKey.
     * @param         key
     * @return         Alley
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException
     */
    public Alley getAlley(AlleyPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "AlleyBusinessDelegate:getAlley - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        Alley returnBO = null;

        AlleyDAO dao = getAlleyDAO();

        try {
            returnBO = dao.findAlley(key);
        } catch (Exception exc) {
            String errMsg = "AlleyBusinessDelegate:getAlley( AlleyPrimaryKey key ) - unable to locate Alley with key " +
                key.toString() + " - " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseAlleyDAO(dao);
        }

        return returnBO;
    }

    /**
     * Method to retrieve a collection of all Alleys
     *
     * @return         ArrayList<Alley>
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Alley> getAllAlley() throws ProcessingException {
        String msgPrefix = "AlleyBusinessDelegate:getAllAlley() - ";
        ArrayList<Alley> array = null;

        AlleyDAO dao = getAlleyDAO();

        try {
            array = dao.findAllAlley();
        } catch (Exception exc) {
            String errMsg = msgPrefix + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseAlleyDAO(dao);
        }

        return array;
    }

    /**
     * Creates the provided BO.
     * @param                businessObject         Alley
     * @return       Alley
     * @exception    ProcessingException
     * @exception        IllegalArgumentException
     */
    public Alley createAlley(Alley businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "AlleyBusinessDelegate:createAlley - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // return value once persisted
        AlleyDAO dao = getAlleyDAO();

        try {
            businessObject = dao.createAlley(businessObject);
        } catch (Exception exc) {
            String errMsg = "AlleyBusinessDelegate:createAlley() - Unable to create Alley" +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseAlleyDAO(dao);
        }

        return (businessObject);
    }

    /**
     * Saves the underlying BO.
     * @param                businessObject                Alley
     * @return       what was just saved
     * @exception    ProcessingException
     * @exception          IllegalArgumentException
     */
    public Alley saveAlley(Alley businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "AlleyBusinessDelegate:saveAlley - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AlleyPrimaryKey key = businessObject.getAlleyPrimaryKey();

        if (key != null) {
            AlleyDAO dao = getAlleyDAO();

            try {
                businessObject = (Alley) dao.saveAlley(businessObject);
            } catch (Exception exc) {
                String errMsg = "AlleyBusinessDelegate:saveAlley() - Unable to save Alley" +
                    exc;
                LOGGER.warning(errMsg);
                throw new ProcessingException(errMsg);
            } finally {
                releaseAlleyDAO(dao);
            }
        } else {
            String errMsg = "AlleyBusinessDelegate:saveAlley() - Unable to create Alley due to it having a null AlleyPrimaryKey.";

            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        }

        return (businessObject);
    }

    /**
     * Deletes the associatied value object using the provided primary key.
     * @param                key         AlleyPrimaryKey
     * @exception         ProcessingException
     */
    public void delete(AlleyPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "AlleyBusinessDelegate:saveAlley - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        AlleyDAO dao = getAlleyDAO();

        try {
            dao.deleteAlley(key);
        } catch (Exception exc) {
            String errMsg = msgPrefix + "Unable to delete Alley using key = " +
                key + ". " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseAlleyDAO(dao);
        }

        return;
    }

    // business methods
    /**
     * Returns the Alley specific DAO.
     *
     * @return      Alley DAO
     */
    public AlleyDAO getAlleyDAO() {
        return (new com.occulue.dao.AlleyDAO());
    }

    /**
     * Release the AlleyDAO back to the FrameworkDAOFactory
     */
    public void releaseAlleyDAO(com.occulue.dao.AlleyDAO dao) {
        dao = null;
    }
}
