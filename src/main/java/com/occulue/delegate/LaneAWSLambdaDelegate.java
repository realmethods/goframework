/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.amazonaws.services.lambda.runtime.Context;

import com.occulue.bo.*;

import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

import com.occulue.primarykey.*;

import io.swagger.annotations.*;

import java.io.IOException;

import java.util.*;

import javax.ws.rs.*;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;


/**
 * Lane AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Lane related services in the case of a Lane business related service failing.</li>
 * <li>Exposes a simpler, uniform Lane interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Lane business related services.</li>
 * </ol>
 * <p>
 * @author
 */
@Api(value = "Lane", description = "RESTful API to interact with Lane resources.")
@Path("/Lane")
public class LaneAWSLambdaDelegate extends BaseAWSLambdaDelegate {
    // role related methods

    //************************************************************************
    // Attributes
    //************************************************************************

    //    private static final Logger LOGGER = Logger.getLogger(LaneAWSLambdaDelegate.class.getName());
    private static final String PACKAGE_NAME = "Lane";

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public LaneAWSLambdaDelegate() {
    }

    /**
     * Creates the provided Lane
     * @param                businessObject         Lane
         * @param                context                Context
     * @return             Lane
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Lane", notes = "Creates Lane using the provided data")
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Lane createLane(
        @ApiParam(value = "Lane entity to create", required = true)
    Lane businessObject, Context context) throws CreationException {
        if (businessObject == null) {
            String errMsg = "Null Lane provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        }

        try {
            String actionName = "save";
            String result = call(PACKAGE_NAME, actionName, businessObject);
            businessObject = (Lane) fromJson(result, Lane.class);
        } catch (Exception exc) {
            String errMsg = "LaneAWSLambdaDelegate:createLane() - Unable to create Lane" +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        } finally {
        }

        return (businessObject);
    }

    /**
     * Method to retrieve the Lane via a supplied LanePrimaryKey.
     * @param         key
         * @param        context                Context
     * @return         Lane
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Lane", notes = "Gets the Lane associated with the provided primary key", response = Lane.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public static Lane getLane(
        @ApiParam(value = "Lane primary key", required = true)
    LanePrimaryKey key, Context context) throws NotFoundException {
        Lane businessObject = null;

        try {
            String actionName = "load";
            String result = call(PACKAGE_NAME, actionName, key);
            businessObject = (Lane) fromJson(result, Lane.class);
        } catch (Exception exc) {
            String errMsg = "Unable to locate Lane with key " + key.toString() +
                " - " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return businessObject;
    }

    /**
     * Saves the provided Lane
     * @param                businessObject                Lane
         * @param                context                Context
     * @return       what was just saved
     * @exception    SaveException
     */
    @ApiOperation(value = "Saves a Lane", notes = "Saves Lane using the provided data")
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Lane saveLane(
        @ApiParam(value = "Lane entity to save", required = true)
    Lane businessObject, Context context) throws SaveException {
        if (businessObject == null) {
            String errMsg = "Null Lane provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        LanePrimaryKey key = businessObject.getLanePrimaryKey();

        if (key != null) {
            try {
                String actionName = "save";
                String result = call(PACKAGE_NAME, actionName, businessObject);
                businessObject = (Lane) fromJson(result, Lane.class);
            } catch (Exception exc) {
                String errMsg = "Unable to save Lane" +
                    getContextDetails(context) + exc;
                context.getLogger().log(errMsg);
                throw new SaveException(errMsg);
            } finally {
            }
        } else {
            String errMsg = "Unable to create Lane due to it having a null LanePrimaryKey.";
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        return (businessObject);
    }

    /**
    * Method to retrieve a collection of all Lanes
    * @param                context                Context
    * @return         ArrayList<Lane>
    */
    @ApiOperation(value = "Get all Lane", notes = "Get all Lane from storage", responseContainer = "ArrayList", response = Lane.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public static ArrayList<Lane> getAllLane(Context context)
        throws NotFoundException {
        ArrayList<Lane> array = null;

        try {
            String actionName = "viewAll";
            String result = call(PACKAGE_NAME, actionName, null);
            array = (ArrayList<Lane>) fromJson(result, ArrayList.class);
        } catch (Exception exc) {
            String errMsg = "failed to getAllLane - " +
                getContextDetails(context) + exc.getMessage();
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return array;
    }

    /**
     * Deletes the associated business object using the provided primary key.
     * @param                key         LanePrimaryKey
     * @param                context                Context
     * @exception         DeletionException
     */
    @ApiOperation(value = "Deletes a Lane", notes = "Deletes the Lane associated with the provided primary key", response = Lane.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public static void deleteLane(
        @ApiParam(value = "Lane primary key", required = true)
    LanePrimaryKey key, Context context) throws DeletionException {
        if (key == null) {
            String errMsg = "Null key provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        }

        try {
            String actionName = "delete";
            String result = call(PACKAGE_NAME, actionName, key);
        } catch (Exception exc) {
            String errMsg = "Unable to delete Lane using key = " + key + ". " +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        } finally {
        }

        return;
    }
}
