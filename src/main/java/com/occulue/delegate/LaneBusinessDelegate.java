/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.*;

import com.occulue.primarykey.*;

import java.io.IOException;

import java.util.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Lane business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Lane related services in the case of a Lane business related service failing.</li>
 * <li>Exposes a simpler, uniform Lane interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Lane business related services.</li>
 * </ol>
 * <p>
 * @author
 */
public class LaneBusinessDelegate extends BaseBusinessDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    /**
     * Singleton instance
     */
    protected static LaneBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Lane.class.getName());

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public LaneBusinessDelegate() {
    }

    /**
         * Lane Business Delegate Factory Method
         *
         * Returns a singleton instance of LaneBusinessDelegate().
         * All methods are expected to be self-sufficient.
         *
         * @return         LaneBusinessDelegate
         */
    public static LaneBusinessDelegate getLaneInstance() {
        if (singleton == null) {
            singleton = new LaneBusinessDelegate();
        }

        return (singleton);
    }

    /**
     * Method to retrieve the Lane via an LanePrimaryKey.
     * @param         key
     * @return         Lane
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException
     */
    public Lane getLane(LanePrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "LaneBusinessDelegate:getLane - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        Lane returnBO = null;

        LaneDAO dao = getLaneDAO();

        try {
            returnBO = dao.findLane(key);
        } catch (Exception exc) {
            String errMsg = "LaneBusinessDelegate:getLane( LanePrimaryKey key ) - unable to locate Lane with key " +
                key.toString() + " - " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseLaneDAO(dao);
        }

        return returnBO;
    }

    /**
     * Method to retrieve a collection of all Lanes
     *
     * @return         ArrayList<Lane>
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Lane> getAllLane() throws ProcessingException {
        String msgPrefix = "LaneBusinessDelegate:getAllLane() - ";
        ArrayList<Lane> array = null;

        LaneDAO dao = getLaneDAO();

        try {
            array = dao.findAllLane();
        } catch (Exception exc) {
            String errMsg = msgPrefix + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseLaneDAO(dao);
        }

        return array;
    }

    /**
     * Creates the provided BO.
     * @param                businessObject         Lane
     * @return       Lane
     * @exception    ProcessingException
     * @exception        IllegalArgumentException
     */
    public Lane createLane(Lane businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "LaneBusinessDelegate:createLane - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // return value once persisted
        LaneDAO dao = getLaneDAO();

        try {
            businessObject = dao.createLane(businessObject);
        } catch (Exception exc) {
            String errMsg = "LaneBusinessDelegate:createLane() - Unable to create Lane" +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseLaneDAO(dao);
        }

        return (businessObject);
    }

    /**
     * Saves the underlying BO.
     * @param                businessObject                Lane
     * @return       what was just saved
     * @exception    ProcessingException
     * @exception          IllegalArgumentException
     */
    public Lane saveLane(Lane businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "LaneBusinessDelegate:saveLane - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        LanePrimaryKey key = businessObject.getLanePrimaryKey();

        if (key != null) {
            LaneDAO dao = getLaneDAO();

            try {
                businessObject = (Lane) dao.saveLane(businessObject);
            } catch (Exception exc) {
                String errMsg = "LaneBusinessDelegate:saveLane() - Unable to save Lane" +
                    exc;
                LOGGER.warning(errMsg);
                throw new ProcessingException(errMsg);
            } finally {
                releaseLaneDAO(dao);
            }
        } else {
            String errMsg = "LaneBusinessDelegate:saveLane() - Unable to create Lane due to it having a null LanePrimaryKey.";

            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        }

        return (businessObject);
    }

    /**
     * Deletes the associatied value object using the provided primary key.
     * @param                key         LanePrimaryKey
     * @exception         ProcessingException
     */
    public void delete(LanePrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "LaneBusinessDelegate:saveLane - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        LaneDAO dao = getLaneDAO();

        try {
            dao.deleteLane(key);
        } catch (Exception exc) {
            String errMsg = msgPrefix + "Unable to delete Lane using key = " +
                key + ". " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseLaneDAO(dao);
        }

        return;
    }

    // business methods
    /**
     * Returns the Lane specific DAO.
     *
     * @return      Lane DAO
     */
    public LaneDAO getLaneDAO() {
        return (new com.occulue.dao.LaneDAO());
    }

    /**
     * Release the LaneDAO back to the FrameworkDAOFactory
     */
    public void releaseLaneDAO(com.occulue.dao.LaneDAO dao) {
        dao = null;
    }
}
