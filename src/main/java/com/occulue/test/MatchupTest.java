/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test Matchup class.
 *
 * @author
 */
public class MatchupTest extends BaseTest {
    // attributes 
    protected MatchupPrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(Matchup.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public MatchupTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public MatchupTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new MatchupTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a Matchup, through a MatchupTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on MatchupTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("<b>Successfully ran a full test on MatchupTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new Matchup.
     *
     * @return    Matchup
     */
    public Matchup testCreate() throws Throwable {
        Matchup businessObject = null;

        {
            LOGGER.info("MatchupTest:testCreate()");
            LOGGER.info("-- Attempting to create a Matchup");

            StringBuilder msg = new StringBuilder(
                    "-- Failed to create a Matchup");

            try {
                businessObject = MatchupBusinessDelegate.getMatchupInstance()
                                                        .createMatchup(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (MatchupPrimaryKey) businessObject.getMatchupPrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info(
                    "-- Successfully created a Matchup with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a Matchup.
     *
     * @return    Matchup
     */
    public Matchup testRead() throws Throwable {
        LOGGER.info("MatchupTest:testRead()");
        LOGGER.info("-- Reading a previously created Matchup");

        Matchup businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read Matchup with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = MatchupBusinessDelegate.getMatchupInstance()
                                                    .getMatchup(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found Matchup " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a Matchup.
     *
     * @return    Matchup
     */
    public Matchup testUpdate() throws Throwable {
        LOGGER.info("MatchupTest:testUpdate()");
        LOGGER.info("-- Attempting to update a Matchup.");

        StringBuilder msg = new StringBuilder("Failed to update a Matchup : ");
        Matchup businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created Matchup.");

            // for use later on...
            thePrimaryKey = (MatchupPrimaryKey) businessObject.getMatchupPrimaryKey();

            MatchupBusinessDelegate proxy = MatchupBusinessDelegate.getMatchupInstance();
            businessObject = proxy.saveMatchup(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved Matchup - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a Matchup.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("MatchupTest:testDelete()");
        LOGGER.info("-- Deleting a previously created Matchup.");

        try {
            MatchupBusinessDelegate.getMatchupInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted Matchup with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete Matchup with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Matchups.
     *
     * @return    Collection
     */
    public ArrayList<Matchup> testGetAll() throws Throwable {
        LOGGER.info(
            "MatchupTest:testGetAll() - Retrieving Collection of Matchups:");

        StringBuilder msg = new StringBuilder("-- Failed to get all Matchup : ");
        ArrayList<Matchup> collection = null;

        try {
            // call the static get method on the MatchupBusinessDelegate
            collection = MatchupBusinessDelegate.getMatchupInstance()
                                                .getAllMatchup();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                Matchup currentBO = null;
                Iterator<Matchup> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getMatchupPrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate Matchup
     *
     * @return    Matchup
     */
    protected Matchup getNewBO() {
        Matchup newBO = new Matchup();

        // AIB : \#defaultBOOutput() 
        newBO.setName(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));

        // ~AIB
        return (newBO);
    }
}
