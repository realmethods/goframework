/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test Alley class.
 *
 * @author
 */
public class AlleyTest extends BaseTest {
    // attributes 
    protected AlleyPrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(Alley.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public AlleyTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public AlleyTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new AlleyTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a Alley, through a AlleyTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on AlleyTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("<b>Successfully ran a full test on AlleyTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new Alley.
     *
     * @return    Alley
     */
    public Alley testCreate() throws Throwable {
        Alley businessObject = null;

        {
            LOGGER.info("AlleyTest:testCreate()");
            LOGGER.info("-- Attempting to create a Alley");

            StringBuilder msg = new StringBuilder("-- Failed to create a Alley");

            try {
                businessObject = AlleyBusinessDelegate.getAlleyInstance()
                                                      .createAlley(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (AlleyPrimaryKey) businessObject.getAlleyPrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info("-- Successfully created a Alley with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a Alley.
     *
     * @return    Alley
     */
    public Alley testRead() throws Throwable {
        LOGGER.info("AlleyTest:testRead()");
        LOGGER.info("-- Reading a previously created Alley");

        Alley businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read Alley with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = AlleyBusinessDelegate.getAlleyInstance()
                                                  .getAlley(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found Alley " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a Alley.
     *
     * @return    Alley
     */
    public Alley testUpdate() throws Throwable {
        LOGGER.info("AlleyTest:testUpdate()");
        LOGGER.info("-- Attempting to update a Alley.");

        StringBuilder msg = new StringBuilder("Failed to update a Alley : ");
        Alley businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created Alley.");

            // for use later on...
            thePrimaryKey = (AlleyPrimaryKey) businessObject.getAlleyPrimaryKey();

            AlleyBusinessDelegate proxy = AlleyBusinessDelegate.getAlleyInstance();
            businessObject = proxy.saveAlley(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved Alley - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a Alley.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("AlleyTest:testDelete()");
        LOGGER.info("-- Deleting a previously created Alley.");

        try {
            AlleyBusinessDelegate.getAlleyInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted Alley with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete Alley with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Alleys.
     *
     * @return    Collection
     */
    public ArrayList<Alley> testGetAll() throws Throwable {
        LOGGER.info("AlleyTest:testGetAll() - Retrieving Collection of Alleys:");

        StringBuilder msg = new StringBuilder("-- Failed to get all Alley : ");
        ArrayList<Alley> collection = null;

        try {
            // call the static get method on the AlleyBusinessDelegate
            collection = AlleyBusinessDelegate.getAlleyInstance().getAllAlley();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                Alley currentBO = null;
                Iterator<Alley> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getAlleyPrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate Alley
     *
     * @return    Alley
     */
    protected Alley getNewBO() {
        Alley newBO = new Alley();

        // AIB : \#defaultBOOutput() 
        newBO.setName(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));

        // ~AIB
        return (newBO);
    }
}
