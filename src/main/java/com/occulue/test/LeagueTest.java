/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test League class.
 *
 * @author
 */
public class LeagueTest extends BaseTest {
    // attributes 
    protected LeaguePrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(League.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public LeagueTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public LeagueTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new LeagueTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a League, through a LeagueTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on LeagueTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("<b>Successfully ran a full test on LeagueTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new League.
     *
     * @return    League
     */
    public League testCreate() throws Throwable {
        League businessObject = null;

        {
            LOGGER.info("LeagueTest:testCreate()");
            LOGGER.info("-- Attempting to create a League");

            StringBuilder msg = new StringBuilder(
                    "-- Failed to create a League");

            try {
                businessObject = LeagueBusinessDelegate.getLeagueInstance()
                                                       .createLeague(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (LeaguePrimaryKey) businessObject.getLeaguePrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info("-- Successfully created a League with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a League.
     *
     * @return    League
     */
    public League testRead() throws Throwable {
        LOGGER.info("LeagueTest:testRead()");
        LOGGER.info("-- Reading a previously created League");

        League businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read League with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = LeagueBusinessDelegate.getLeagueInstance()
                                                   .getLeague(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found League " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a League.
     *
     * @return    League
     */
    public League testUpdate() throws Throwable {
        LOGGER.info("LeagueTest:testUpdate()");
        LOGGER.info("-- Attempting to update a League.");

        StringBuilder msg = new StringBuilder("Failed to update a League : ");
        League businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created League.");

            // for use later on...
            thePrimaryKey = (LeaguePrimaryKey) businessObject.getLeaguePrimaryKey();

            LeagueBusinessDelegate proxy = LeagueBusinessDelegate.getLeagueInstance();
            businessObject = proxy.saveLeague(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved League - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a League.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("LeagueTest:testDelete()");
        LOGGER.info("-- Deleting a previously created League.");

        try {
            LeagueBusinessDelegate.getLeagueInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted League with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete League with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Leagues.
     *
     * @return    Collection
     */
    public ArrayList<League> testGetAll() throws Throwable {
        LOGGER.info(
            "LeagueTest:testGetAll() - Retrieving Collection of Leagues:");

        StringBuilder msg = new StringBuilder("-- Failed to get all League : ");
        ArrayList<League> collection = null;

        try {
            // call the static get method on the LeagueBusinessDelegate
            collection = LeagueBusinessDelegate.getLeagueInstance()
                                               .getAllLeague();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                League currentBO = null;
                Iterator<League> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getLeaguePrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate League
     *
     * @return    League
     */
    protected League getNewBO() {
        League newBO = new League();

        // AIB : \#defaultBOOutput() 
        newBO.setName(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));

        // ~AIB
        return (newBO);
    }
}
