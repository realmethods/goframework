/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test Tournament class.
 *
 * @author
 */
public class TournamentTest extends BaseTest {
    // attributes 
    protected TournamentPrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(Tournament.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public TournamentTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public TournamentTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new TournamentTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a Tournament, through a TournamentTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on TournamentTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info(
                "<b>Successfully ran a full test on TournamentTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new Tournament.
     *
     * @return    Tournament
     */
    public Tournament testCreate() throws Throwable {
        Tournament businessObject = null;

        {
            LOGGER.info("TournamentTest:testCreate()");
            LOGGER.info("-- Attempting to create a Tournament");

            StringBuilder msg = new StringBuilder(
                    "-- Failed to create a Tournament");

            try {
                businessObject = TournamentBusinessDelegate.getTournamentInstance()
                                                           .createTournament(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (TournamentPrimaryKey) businessObject.getTournamentPrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info(
                    "-- Successfully created a Tournament with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a Tournament.
     *
     * @return    Tournament
     */
    public Tournament testRead() throws Throwable {
        LOGGER.info("TournamentTest:testRead()");
        LOGGER.info("-- Reading a previously created Tournament");

        Tournament businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read Tournament with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = TournamentBusinessDelegate.getTournamentInstance()
                                                       .getTournament(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found Tournament " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a Tournament.
     *
     * @return    Tournament
     */
    public Tournament testUpdate() throws Throwable {
        LOGGER.info("TournamentTest:testUpdate()");
        LOGGER.info("-- Attempting to update a Tournament.");

        StringBuilder msg = new StringBuilder(
                "Failed to update a Tournament : ");
        Tournament businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created Tournament.");

            // for use later on...
            thePrimaryKey = (TournamentPrimaryKey) businessObject.getTournamentPrimaryKey();

            TournamentBusinessDelegate proxy = TournamentBusinessDelegate.getTournamentInstance();
            businessObject = proxy.saveTournament(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved Tournament - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a Tournament.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("TournamentTest:testDelete()");
        LOGGER.info("-- Deleting a previously created Tournament.");

        try {
            TournamentBusinessDelegate.getTournamentInstance()
                                      .delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted Tournament with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete Tournament with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Tournaments.
     *
     * @return    Collection
     */
    public ArrayList<Tournament> testGetAll() throws Throwable {
        LOGGER.info(
            "TournamentTest:testGetAll() - Retrieving Collection of Tournaments:");

        StringBuilder msg = new StringBuilder(
                "-- Failed to get all Tournament : ");
        ArrayList<Tournament> collection = null;

        try {
            // call the static get method on the TournamentBusinessDelegate
            collection = TournamentBusinessDelegate.getTournamentInstance()
                                                   .getAllTournament();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                Tournament currentBO = null;
                Iterator<Tournament> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getTournamentPrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate Tournament
     *
     * @return    Tournament
     */
    protected Tournament getNewBO() {
        Tournament newBO = new Tournament();

        // AIB : \#defaultBOOutput() 
        newBO.setName(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));
        newBO.setType(TournamentType.getDefaultValue());

        // ~AIB
        return (newBO);
    }
}
