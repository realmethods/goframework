/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test Lane class.
 *
 * @author
 */
public class LaneTest extends BaseTest {
    // attributes 
    protected LanePrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(Lane.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public LaneTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public LaneTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new LaneTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a Lane, through a LaneTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on LaneTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("<b>Successfully ran a full test on LaneTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new Lane.
     *
     * @return    Lane
     */
    public Lane testCreate() throws Throwable {
        Lane businessObject = null;

        {
            LOGGER.info("LaneTest:testCreate()");
            LOGGER.info("-- Attempting to create a Lane");

            StringBuilder msg = new StringBuilder("-- Failed to create a Lane");

            try {
                businessObject = LaneBusinessDelegate.getLaneInstance()
                                                     .createLane(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (LanePrimaryKey) businessObject.getLanePrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info("-- Successfully created a Lane with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a Lane.
     *
     * @return    Lane
     */
    public Lane testRead() throws Throwable {
        LOGGER.info("LaneTest:testRead()");
        LOGGER.info("-- Reading a previously created Lane");

        Lane businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read Lane with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = LaneBusinessDelegate.getLaneInstance()
                                                 .getLane(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found Lane " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a Lane.
     *
     * @return    Lane
     */
    public Lane testUpdate() throws Throwable {
        LOGGER.info("LaneTest:testUpdate()");
        LOGGER.info("-- Attempting to update a Lane.");

        StringBuilder msg = new StringBuilder("Failed to update a Lane : ");
        Lane businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created Lane.");

            // for use later on...
            thePrimaryKey = (LanePrimaryKey) businessObject.getLanePrimaryKey();

            LaneBusinessDelegate proxy = LaneBusinessDelegate.getLaneInstance();
            businessObject = proxy.saveLane(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved Lane - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a Lane.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("LaneTest:testDelete()");
        LOGGER.info("-- Deleting a previously created Lane.");

        try {
            LaneBusinessDelegate.getLaneInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted Lane with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete Lane with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Lanes.
     *
     * @return    Collection
     */
    public ArrayList<Lane> testGetAll() throws Throwable {
        LOGGER.info("LaneTest:testGetAll() - Retrieving Collection of Lanes:");

        StringBuilder msg = new StringBuilder("-- Failed to get all Lane : ");
        ArrayList<Lane> collection = null;

        try {
            // call the static get method on the LaneBusinessDelegate
            collection = LaneBusinessDelegate.getLaneInstance().getAllLane();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                Lane currentBO = null;
                Iterator<Lane> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getLanePrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate Lane
     *
     * @return    Lane
     */
    protected Lane getNewBO() {
        Lane newBO = new Lane();

        // AIB : \#defaultBOOutput() 
        newBO.setNumber(new java.lang.Integer(
                new String(
                    org.apache.commons.lang3.RandomStringUtils.randomNumeric(3))));

        // ~AIB
        return (newBO);
    }
}
