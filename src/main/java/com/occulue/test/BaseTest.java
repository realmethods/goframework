/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import junit.framework.*;

import java.util.logging.*;


/**
 * Base class for application Test classes.
 *
 * @author
 */
public class BaseTest extends TestCase {
    /**
     * Overloaded constructor
     *
     * @param        testCaseName        String
     */
    protected BaseTest(String testCaseName) {
        super(testCaseName);
    }

    public static void runTheTest(Handler logHandler) {
        TestSuite suite = new TestSuite();

        suite.addTest(new PlayerTest("CRUD", logHandler));
        suite.addTest(new LeagueTest("CRUD", logHandler));
        suite.addTest(new TournamentTest("CRUD", logHandler));
        suite.addTest(new MatchupTest("CRUD", logHandler));
        suite.addTest(new GameTest("CRUD", logHandler));
        suite.addTest(new AlleyTest("CRUD", logHandler));
        suite.addTest(new LaneTest("CRUD", logHandler));
        junit.textui.TestRunner.run(suite);
    }
}
