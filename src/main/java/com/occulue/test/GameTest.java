/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;

import com.occulue.test.*;

import junit.framework.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test Game class.
 *
 * @author
 */
public class GameTest extends BaseTest {
    // attributes 
    protected GamePrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(Game.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public GameTest(String testCaseName) {
        super(testCaseName);
    }

    /**
     * construtor
     *
     * @param    testCaseName                unique name of the TestCase, among others in the same test suite
     */
    public GameTest(String testCaseName, Handler handler) {
        super(testCaseName);

        this.handler = handler;
        LOGGER.addHandler(this.handler);
    }

    // TestCase overloads
    /**
     * Overload from JUnit TestCase, providing the method name(s) to invoke for testing.
     * Only have CRUD as part of the testing since read,update,delete rely on create.  The
     * other methods can be invoked on their own, however, if the data repository in use
     * is populated correctly.  This version of the overload assumes no data for the corresponding
     * businessObject is in the repository.
     *
     * @return    junit.framework.Test
     */
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new GameTest("CRUD"));

        return suite;
    }

    /**
     * Overload from JUnit TestCase, called once before a test is executed.
     *
     * @return    junit.framework.Test
     */
    protected void setUp() throws java.lang.Exception {
    }

    // test methods

    /**
     * Full Create-Read-Update-Delete of a Game, through a GameTest.
     */
    public void CRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("<b>Beginning full test on GameTest...</b>");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("<b>Successfully ran a full test on GameTest...</b>");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            handler.flush();
            LOGGER.removeHandler(handler);
        }
    }

    /**
     * Tests creating a new Game.
     *
     * @return    Game
     */
    public Game testCreate() throws Throwable {
        Game businessObject = null;

        {
            LOGGER.info("GameTest:testCreate()");
            LOGGER.info("-- Attempting to create a Game");

            StringBuilder msg = new StringBuilder("-- Failed to create a Game");

            try {
                businessObject = GameBusinessDelegate.getGameInstance()
                                                     .createGame(getNewBO());
                assertNotNull(msg.toString(), businessObject);

                thePrimaryKey = (GamePrimaryKey) businessObject.getGamePrimaryKey();
                assertNotNull(msg.toString() + "Contains a null primary key",
                    thePrimaryKey);

                LOGGER.info("-- Successfully created a Game with primary key" +
                    thePrimaryKey);
            } catch (AssertionFailedError afe) {
                LOGGER.severe(afe.toString());
                throw afe;
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a Game.
     *
     * @return    Game
     */
    public Game testRead() throws Throwable {
        LOGGER.info("GameTest:testRead()");
        LOGGER.info("-- Reading a previously created Game");

        Game businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read Game with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = GameBusinessDelegate.getGameInstance()
                                                 .getGame(thePrimaryKey);

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Successfully found Game " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a Game.
     *
     * @return    Game
     */
    public Game testUpdate() throws Throwable {
        LOGGER.info("GameTest:testUpdate()");
        LOGGER.info("-- Attempting to update a Game.");

        StringBuilder msg = new StringBuilder("Failed to update a Game : ");
        Game businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(msg.toString(), businessObject);

            LOGGER.info("-- Now updating the created Game.");

            // for use later on...
            thePrimaryKey = (GamePrimaryKey) businessObject.getGamePrimaryKey();

            GameBusinessDelegate proxy = GameBusinessDelegate.getGameInstance();
            businessObject = proxy.saveGame(businessObject);

            assertNotNull(msg.toString() + " ", businessObject);

            LOGGER.info("-- Successfully saved Game - " +
                businessObject.toString());
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a Game.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("GameTest:testDelete()");
        LOGGER.info("-- Deleting a previously created Game.");

        try {
            GameBusinessDelegate.getGameInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted Game with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete Game with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Games.
     *
     * @return    Collection
     */
    public ArrayList<Game> testGetAll() throws Throwable {
        LOGGER.info("GameTest:testGetAll() - Retrieving Collection of Games:");

        StringBuilder msg = new StringBuilder("-- Failed to get all Game : ");
        ArrayList<Game> collection = null;

        try {
            // call the static get method on the GameBusinessDelegate
            collection = GameBusinessDelegate.getGameInstance().getAllGame();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                Game currentBO = null;
                Iterator<Game> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull("-- null value object in Collection.",
                        currentBO);
                    assertNotNull("-- value object in Collection has a null primary key",
                        currentBO.getGamePrimaryKey());

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (AssertionFailedError afe) {
            LOGGER.severe(afe.toString());
            throw afe;
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    /**
     * Returns a new populate Game
     *
     * @return    Game
     */
    protected Game getNewBO() {
        Game newBO = new Game();

        // AIB : \#defaultBOOutput() 
        newBO.setFrames(new java.lang.Integer(
                new String(
                    org.apache.commons.lang3.RandomStringUtils.randomNumeric(3))));

        // ~AIB
        return (newBO);
    }
}
