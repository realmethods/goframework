/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import org.hibernate.HibernateException;

import org.hibernate.usertype.UserType;

import java.util.*;


/**
 * TournamentType enumerator class.
 *
 * Enumerated types are handled on behalf of Hiberate as VARCHARs.  The necessary
 * methods that implement Hibernat's UserType interface assume that Enumerated
 * types contain one or more values, each named uniquely and declared (modeled) with
 * order, although the order is assumed.
 *
// AIB : #enumDocumentation()
     * Encapsulates data for business entity TournamentType.
    // ~AIB
 * @author
 */
public class TournamentTypeUserType extends FrameworkHibernateEnumerator<com.occulue.bo.TournamentType>
    implements java.io.Serializable {
    //************************************************************************
    // Constructors
    //************************************************************************
    /**
     * Default Constructor - for reflection purposes only
     */
    public TournamentTypeUserType() {
        super(com.occulue.bo.TournamentType.class);
    }
}
